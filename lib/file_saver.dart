import 'dart:io';

class FileSaver {
  /// Получение корректного названия файла для сохранения
  static Future<String?> returningCorrectName(File file, String folderToSave) async {
    //проверка на существование файла
    final checkPathExistence =
        await File("$folderToSave${file.path}").exists();
    if (checkPathExistence) {
      //изменение, если существует
      final newFileName = _fileRenamer(file.path);
      return newFileName;
    }
    return file.path;
  }

  static String? _fileRenamer(String path) {
    //Содержит ли "(number)"
    RegExp exp = RegExp(r'\s\(\d+\)');
    RegExpMatch? match = exp.firstMatch(path);
    String result;
    if (match == null) {
      //Если нет паттерна
      RegExp dot = RegExp(r'[.]');
      result = path.replaceFirst(dot, ' (1).');
    } else {
      //Если есть паттерн, то ++1
      int number =
          int.parse(path.substring(path.indexOf("(") + 1, path.indexOf(")")));
      number++;
      result = path.replaceAll(exp, ' ($number)');
    }

    return result;
  }
}
