import 'dart:io';
import 'package:dart_testing/file_saver.dart';
import 'package:test/test.dart';

void main() {
  test('standart new file', () async {
    String path = "standart.txt";
    final txtFile = File(path);
    final test = await FileSaver.returningCorrectName(txtFile,
        "${Directory.current.path}${Platform.pathSeparator}testing_folder${Platform.pathSeparator}");
    expect(test, "standart (1).txt");
    print("\nI: $path");
    print("O: $test");
  });

  test('numbered standart file', () async {
    String path = "standart (1).txt";
    final txtFile = File(path);
    final test = await FileSaver.returningCorrectName(txtFile,
        "${Directory.current.path}${Platform.pathSeparator}testing_folder${Platform.pathSeparator}");
    expect(test, "standart (2).txt");
    print("\nI: $path");
    print("O: $test");
  });

  test('nonexisting file', () async {
    String path = "nonexisting.txt";
    final txtFile = File(path);
    final test = await FileSaver.returningCorrectName(txtFile,
        "${Directory.current.path}${Platform.pathSeparator}testing_folder${Platform.pathSeparator}");
    expect(test, "nonexisting.txt");
    print("\nI: $path");
    print("O: $test");
  });

  test('numbered nonexisting file', () async {
    String path = "nonexisting (1).txt";
    final txtFile = File(path);
    final test = await FileSaver.returningCorrectName(txtFile,
        "${Directory.current.path}${Platform.pathSeparator}testing_folder${Platform.pathSeparator}");
    expect(test, "nonexisting (1).txt");
    print("\nI: $path");
    print("O: $test");
  });

  test('file with .tar.gz', () async {
    String path = "arh.tar.gz";
    final txtFile = File(path);
    final test = await FileSaver.returningCorrectName(txtFile,
        "${Directory.current.path}${Platform.pathSeparator}testing_folder${Platform.pathSeparator}");
    expect(test, "arh (1).tar.gz");
    print("\nI: $path");
    print("O: $test");
  });
  test('numbered file with .tar.gz', () async {
    String path = "arh (1).tar.gz";
    final txtFile = File(path);
    final test = await FileSaver.returningCorrectName(txtFile,
        "${Directory.current.path}${Platform.pathSeparator}testing_folder${Platform.pathSeparator}");
    expect(test, "arh (2).tar.gz");
    print("\nI: $path");
    print("O: $test");
  });

  test('file with several numbers', () async {
    String path = "standart (123).txt";
    final txtFile = File(path);
    final test = await FileSaver.returningCorrectName(txtFile,
        "${Directory.current.path}${Platform.pathSeparator}testing_folder${Platform.pathSeparator}");
    expect(test, "standart (124).txt");
    print("\nI: $path");
    print("O: $test");
  });
}
